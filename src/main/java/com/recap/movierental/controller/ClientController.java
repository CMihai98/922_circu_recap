package com.recap.movierental.controller;

import com.recap.movierental.dto.ClientDTO;
import com.recap.movierental.mappers.ClientMapper;
import com.recap.movierental.service.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ClientController {
    private final ClientMapper clientMapper;
    private final ClientService clientService;

    public ClientController(ClientMapper clientMapper, ClientService clientService) {
        this.clientMapper = clientMapper;
        this.clientService = clientService;
    }

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List<ClientDTO> getClients(){
        return clientMapper.toDtoList(clientService.getClients());
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public ClientDTO addClient(@RequestBody ClientDTO clientDTO){
        return clientMapper.toDto(
                clientService.addClient(
                        clientMapper.fromDto(clientDTO)));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteClient(@PathVariable Long id) {
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/clients", method = RequestMethod.PUT)
    public ClientDTO updateClient(@RequestBody ClientDTO clientDTO){
        return clientMapper.toDto(
          clientService.updateClient(
                  clientMapper.fromDto(clientDTO)));
    }

    @RequestMapping(value = "/clients/{name}", method = RequestMethod.GET)
    public List<ClientDTO> filterClients(@PathVariable String name) {
        return clientMapper.toDtoList(
                clientService.filterClients(name));
    }

    @RequestMapping(value = "/clients/full/{id}", method = RequestMethod.GET)
    public ClientDTO getClientWithRentedMovies(@PathVariable Long id) {
        return clientMapper.toDtoFull(
                clientService.getClientWithRentedMovies(id));
    }
}
