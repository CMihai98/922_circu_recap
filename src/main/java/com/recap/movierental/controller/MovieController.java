package com.recap.movierental.controller;

import com.recap.movierental.dto.MovieDTO;
import com.recap.movierental.mappers.MovieMapper;
import com.recap.movierental.model.Movie;
import com.recap.movierental.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieController {

    private final MovieMapper movieMapper;
    private final MovieService movieService;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public List<MovieDTO> getMovies(){
        return movieMapper.toDtoList(
                movieService.getMovies());
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    public MovieDTO addMovie(@RequestBody MovieDTO movieDTO){
        return movieMapper.toDto(
                movieService.addMovie(
                        movieMapper.fromDto(movieDTO)));
    }

    @RequestMapping(value = "/movies", method = RequestMethod.PUT)
    public MovieDTO updateMovie(@RequestBody MovieDTO movieDTO){
        return movieMapper.toDto(
                movieService.updateMovie(
                        movieMapper.fromDto(movieDTO)));
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMovie(@PathVariable Long id){
        movieService.deleteMovie(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
