package com.recap.movierental.repository;

import com.recap.movierental.model.Client;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {

    List<Client> findClientsByNameContainingOrAddressContaining(String name, String address);

    @EntityGraph(attributePaths = {"rentedMovies"})
    Client findFirstById(Long id);
}
