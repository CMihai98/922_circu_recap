package com.recap.movierental.service;

import com.recap.movierental.model.Client;
import com.recap.movierental.model.Movie;
import com.recap.movierental.repository.ClientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    private final MovieService movieService;

    public ClientService(ClientRepository clientRepository, MovieService movieService) {
        this.clientRepository = clientRepository;
        this.movieService = movieService;
    }

    public List<Client> getClients() {
        return clientRepository.findAll();
    }

    @Transactional
    public Client addClient(Client client){
        return clientRepository.save(client);
    }

    @Transactional
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }

    @Transactional
    public Client updateClient(Client client) {
        clientRepository.findById(client.getId())
                .ifPresent(foundClient -> {
                    foundClient.setName(client.getName());
                    foundClient.setAddress(client.getAddress());
                    foundClient.setAge(client.getAge());
                    client.getRentedMovies().forEach(movie -> {
                        Movie movieToBeAdded = movieService.findMovieById(movie.getId());
                        foundClient.getRentedMovies().add(movieToBeAdded);
                    });
                });
        return client;
    }

    public List<Client> filterClients(String name) {
        return clientRepository.findClientsByNameContainingOrAddressContaining(name, name);
    }

    public Client getClientWithRentedMovies(Long id) {
        return clientRepository.findFirstById(id);
    }
}
