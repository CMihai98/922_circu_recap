package com.recap.movierental.service;

import com.recap.movierental.mappers.MovieMapper;
import com.recap.movierental.model.Movie;
import com.recap.movierental.repository.MovieRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public List<Movie> getMovies(){
        return movieRepository.findAll();
    }

    @Transactional
    public Movie addMovie(Movie movie){
        return this.movieRepository.save(movie);
    }

    @Transactional
    public void deleteMovie(Long id){
        movieRepository.deleteById(id);
    }

    @Transactional
    public Movie updateMovie(Movie movie){
        movieRepository.findById(movie.getId())
                .ifPresent(result -> {
                    result.setTitle(movie.getTitle());
                    result.setCategory(movie.getCategory());
                    result.setRating(movie.getRating());
                });
        return movie;
    }

    @Transactional
    public Movie findMovieById(Long id){
       return movieRepository.findById(id).orElseThrow();
    }
}
