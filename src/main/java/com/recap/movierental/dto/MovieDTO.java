package com.recap.movierental.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieDTO {
    private Long id;
    private String title;
    private String category;
    private int rating;
}
