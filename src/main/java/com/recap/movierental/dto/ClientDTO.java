package com.recap.movierental.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDTO {
    private Long id;
    private String name;
    private String address;
    private int age;
    private Set<MovieDTO> rentedMovies;
}
