package com.recap.movierental.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@Data
@Entity

//@NamedEntityGraph(
//        name = "clientWithMoviesRented",
//        attributeNodes = {
//                @NamedAttributeNode("rentedMovies")
//        })
public class Client extends BaseEntity {
    private String name;
    private String address;
    private int age;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "rentals",
            joinColumns = {@JoinColumn(name = "client_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> rentedMovies = new HashSet<>();
}
