package com.recap.movierental.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@Data
@Entity
public class Movie extends BaseEntity {
    private String title;
    private String category;
    private int rating;

    @ManyToMany(mappedBy = "rentedMovies", fetch = FetchType.LAZY)
    private Set<Client> clients = new HashSet<>();
}
