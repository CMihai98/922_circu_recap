package com.recap.movierental.mappers;

import com.recap.movierental.dto.MovieDTO;
import com.recap.movierental.model.Movie;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface MovieMapper {

    MovieDTO toDto(Movie movie);

    @InheritInverseConfiguration
    Movie fromDto(MovieDTO movieDTO);

    List<MovieDTO> toDtoList(List<Movie> movies);
}
