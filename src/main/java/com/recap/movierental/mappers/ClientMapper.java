package com.recap.movierental.mappers;

import com.recap.movierental.dto.ClientDTO;
import com.recap.movierental.model.Client;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface ClientMapper {

    @Mapping(target = "rentedMovies", ignore = true)
    ClientDTO toDto(Client client);

    @Named(value = "toDtoFull")
    ClientDTO toDtoFull(Client client);

    @InheritInverseConfiguration(name = "toDtoFull")
    Client fromDto(ClientDTO clientDTO);

    List<ClientDTO> toDtoList(List<Client> clients);
}
