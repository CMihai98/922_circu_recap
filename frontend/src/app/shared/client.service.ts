import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Client} from './client.model';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clientsURL = 'http://localhost:8080/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Client[]>(this.clientsURL);
  }

  addClient(client: Client): Observable<Client> {
    return this.httpClient
      .post<Client>(this.clientsURL, client);
  }

  updateClient(client: Client): Observable<Client> {
    return this.httpClient
      .put<Client>(this.clientsURL, client);
  }

  deleteClient(client: Client): Observable<any> {
    const deleteURL = `${this.clientsURL}/${+client.id}`;
    return this.httpClient
      .delete(deleteURL);
  }

  getClientsFiltered(value: string): Observable<Client[]> {
    const filterURL = `${this.clientsURL}/${value}`;
    return this.httpClient
      .get<Client[]>(filterURL);
  }

  getClientWithMovies(client: Client): Observable<Client> {
    const getURL = `${this.clientsURL}/full/${client.id}`;
    return this.httpClient
      .get<Client>(getURL);
  }
}
