export class Movie {
  id: number;
  title: string;
  category: string;
  rating: number;
}
