import {Movie} from './movie.model';

export class Client{
  id: number;
  name: string;
  address: string;
  age: number;
  rentedMovies: Movie[];
}
