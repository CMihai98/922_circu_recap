import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Movie} from './movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  moviesURL = 'http://localhost:8080/api/movies/';

  constructor(private httpClient: HttpClient) {
  }

  getMovies(): Observable<Movie[]> {
    return this.httpClient
      .get<Movie[]>(this.moviesURL);
  }

  addMovie(movie: Movie): Observable<Movie> {
    return this.httpClient
      .post<Movie>(this.moviesURL, movie);
  }

  updateMovie(movie: Movie): Observable<Movie> {
    return this.httpClient
      .put<Movie>(this.moviesURL, movie);
  }

  deleteMovie(movie: Movie): Observable<any> {
    const deleteURL = `${this.moviesURL}/${+movie.id}`;
    return this.httpClient
      .delete(deleteURL);
  }
}
