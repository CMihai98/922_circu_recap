import {Component, Inject, OnInit} from '@angular/core';
import {Client} from '../../shared/client.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export class DialogData {
  client: Client;
}

@Component({
  selector: 'app-clients-update-dialog',
  templateUrl: './clients-update-dialog.component.html',
  styleUrls: ['./clients-update-dialog.component.scss']
})
export class ClientsUpdateDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<ClientsUpdateDialogComponent>) { }

  ngOnInit(): void {
  }

  update(name: string, address: string, age: string): void {
    if (name.length !== 0) {
      this.data.client.name = name;
    }
    if (address.length !== 0) {
      this.data.client.address = address;
    }
    if (age.length !== 0) {
      this.data.client.age = +age;
    }
    this.dialogRef.close(this.data.client);
  }

}
