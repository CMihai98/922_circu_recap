import {Component, Inject, OnInit} from '@angular/core';
import {Client} from '../../shared/client.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MovieService} from '../../shared/movie.service';
import {Movie} from '../../shared/movie.model';
import {ClientService} from '../../shared/client.service';

export class DialogData {
  client: Client;
}

@Component({
  selector: 'app-clients-rent-dialog',
  templateUrl: './clients-rent-dialog.component.html',
  styleUrls: ['./clients-rent-dialog.component.scss']
})
export class ClientsRentDialogComponent implements OnInit {
  movies: Movie[];
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<ClientsRentDialogComponent>,
              private movieService: MovieService,
              private clientService: ClientService) { }

  ngOnInit(): void {
    this.loadMovies();
  }

  loadMovies(): void {
    this.movies = [];
    this.movieService.getMovies()
      .subscribe(allMovies => {
        allMovies.forEach(movie => {
          let found = false;
          this.data.client.rentedMovies.forEach(movie2 => {
            if (movie.id === movie2.id) {
              found = true;
            }});
          if (!found) {
            this.movies.push(movie);
          }
        });
        console.log(this.movies);
      });
  }

  rentMovie(movie: Movie) {
    this.data.client.rentedMovies.push(movie);
    this.clientService.updateClient(this.data.client)
      .subscribe(_ => this.loadMovies());
  }

  close() {
    this.dialogRef.close();
  }
}
