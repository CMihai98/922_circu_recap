import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Client} from '../../shared/client.model';

@Component({
  selector: 'app-clients-add-dialog',
  templateUrl: './clients-add-dialog.component.html',
  styleUrls: ['./clients-add-dialog.component.scss']
})
export class ClientsAddDialogComponent implements OnInit {
  addForm: FormGroup;
  client: Client;

  constructor(private dialogRef: MatDialogRef<ClientsAddDialogComponent>,
              private formBuilder: FormBuilder) {
    this.client = new Client();
  }

  ngOnInit(): void {
    this.initForm();
  }

  add(): void {
    this.dialogRef.close(this.client);
  }

  private initForm() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      age: ['', Validators.min(16)]
    });
    this.addForm.valueChanges.subscribe(values => {
      this.client.name = values.name;
      this.client.address = values.address;
      this.client.age = values.age;
    });
  }
}
