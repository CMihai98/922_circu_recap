import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../shared/client.service';
import {Client} from '../../shared/client.model';
import {MatDialog} from '@angular/material/dialog';
import {ClientsUpdateDialogComponent} from '../clients-update-dialog/clients-update-dialog.component';
import {ClientsAddDialogComponent} from '../clients-add-dialog/clients-add-dialog.component';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ClientsRentDialogComponent} from '../clients-rent-dialog/clients-rent-dialog.component';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {
  clients: Client[];
  clientWithMovies: Client;
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  searchControl = new FormControl();
  constructor(private clientService: ClientService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getClients();
    this.clientWithMovies = new Client();
  }

  getClients(): void {
    this.clientService.getClients()
      .subscribe(clients => {
        this.clients = clients;
        this.clients.forEach(client => {
          this.options.push(client.name);
          this.options.push(client.address);
        });
        this.filteredOptions = this.searchControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value)));
      });
  }

  add(): void {
    this.dialog.open(ClientsAddDialogComponent,
      {width: '300px'})
      .afterClosed().subscribe(client =>
        this.clientService.addClient(client)
          .subscribe(_ => this.getClients()));
  }

  delete(client: Client): void {
    this.clientService.deleteClient(client)
      .subscribe(_ => this.getClients());
  }

  update(client: Client): void {
    this.dialog.open(ClientsUpdateDialogComponent,
      {width: '300px', data: {client}})
      .afterClosed().subscribe(updatedClient =>
        this.clientService.updateClient(updatedClient)
          .subscribe(_ => this.getClients()));
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLocaleLowerCase();

    return this.options.filter(option =>
      option.toLocaleLowerCase().includes(filterValue));
  }

  filter(value: string) {
    this.clientService.getClientsFiltered(value)
      .subscribe(clients => this.clients = clients);
  }

  fetchMovies(client: Client) {
    this.clientService.getClientWithMovies(client)
      .subscribe(result => this.clientWithMovies = result);
  }

  rentMovies() {
    this.dialog.open(ClientsRentDialogComponent,
      {width: '500px', data: {client: this.clientWithMovies}})
      .afterClosed()
      .subscribe(_ => this.fetchMovies(this.clientWithMovies));
  }
}
