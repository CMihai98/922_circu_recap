import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Movie} from '../../shared/movie.model';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-movies-add-dialog',
  templateUrl: './movies-add-dialog.component.html',
  styleUrls: ['./movies-add-dialog.component.scss']
})
export class MoviesAddDialogComponent implements OnInit {
  addForm: FormGroup;
  movie: Movie;
  constructor(private dialogRef: MatDialogRef<MoviesAddDialogComponent>,
              private formBuilder: FormBuilder) {
    this.movie = new Movie();
  }

  ngOnInit(): void {
    this.initForm();
  }

  add(): void {
    this.dialogRef.close(this.movie);
  }

  private initForm() {
    this.addForm = this.formBuilder.group({
      title: ['', Validators.required],
      category: ['', Validators.required],
      rating: ['', Validators.min(0)]
    });
    this.addForm.valueChanges.subscribe(values => {
      this.movie.title = values.title;
      this.movie.category = values.category;
      this.movie.rating = values.rating;
    });
  }
}
