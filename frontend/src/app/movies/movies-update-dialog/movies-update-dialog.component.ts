import {Component, Inject, OnInit} from '@angular/core';
import {Movie} from '../../shared/movie.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export class DialogData {
  movie: Movie;
}

@Component({
  selector: 'app-movies-update-dialog',
  templateUrl: './movies-update-dialog.component.html',
  styleUrls: ['./movies-update-dialog.component.scss']
})
export class MoviesUpdateDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<MoviesUpdateDialogComponent>) { }

  ngOnInit(): void {
  }

  update(title: string, category: string, rating: string): void {
    if (title.length !== 0) {
      this.data.movie.title = title;
    }
    if (category.length !== 0) {
      this.data.movie.category = category;
    }
    if (rating.length !== 0) {
      this.data.movie.rating = +rating;
    }
    this.dialogRef.close(this.data.movie);
  }

}
