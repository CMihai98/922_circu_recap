import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../shared/movie.service';
import {Movie} from '../../shared/movie.model';
import {MatDialog} from '@angular/material/dialog';
import {MoviesAddDialogComponent} from '../movies-add-dialog/movies-add-dialog.component';
import {MoviesUpdateDialogComponent} from '../movies-update-dialog/movies-update-dialog.component';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  movies: Movie[];
  constructor(private movieService: MovieService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getMovies();
  }

  private getMovies() {
    this.movieService.getMovies()
      .subscribe(movies => this.movies = movies);
  }

  delete(movie: Movie): void {
    this.movieService.deleteMovie(movie)
      .subscribe(_ => this.getMovies());
  }

  add(): void {
    this.dialog.open(MoviesAddDialogComponent,
      {width: '300px'})
      .afterClosed().subscribe(movie =>
        this.movieService.addMovie(movie)
          .subscribe(_ => this.getMovies()));
  }

  update(movie: Movie): void {
    this.dialog.open(MoviesUpdateDialogComponent,
      {width: '300px', data: {movie}})
      .afterClosed().subscribe(updateMovie =>
        this.movieService.updateMovie(updateMovie)
          .subscribe(_ => this.getMovies()));
  }
}
