import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientsListComponent } from './clients/clients-list/clients-list.component';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {ClientService} from './shared/client.service';
import {HttpClientModule} from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ClientsUpdateDialogComponent } from './clients/clients-update-dialog/clients-update-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ClientsAddDialogComponent } from './clients/clients-add-dialog/clients-add-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import { MoviesAddDialogComponent } from './movies/movies-add-dialog/movies-add-dialog.component';
import {MovieService} from './shared/movie.service';
import { MoviesUpdateDialogComponent } from './movies/movies-update-dialog/movies-update-dialog.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatListModule} from '@angular/material/list';
import { ClientsRentDialogComponent } from './clients/clients-rent-dialog/clients-rent-dialog.component';
import {MatChipsModule} from '@angular/material/chips';

@NgModule({
  declarations: [
    AppComponent,
    ClientsListComponent,
    MoviesListComponent,
    ClientsUpdateDialogComponent,
    ClientsAddDialogComponent,
    MoviesAddDialogComponent,
    MoviesUpdateDialogComponent,
    ClientsRentDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatExpansionModule,
    MatGridListModule,
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatListModule,
    MatChipsModule
  ],
  providers: [ClientService, MovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
